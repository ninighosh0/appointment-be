const mongoose = require("mongoose");
const { ObjectId } = mongoose.Schema;

const previousAppointmentSchema = new mongoose.Schema({
  doctor: {
    type: ObjectId,
    ref: "Doctor",
  },
  user: {
    type: ObjectId,
    ref: "User",
  },
  time: {
    type: ObjectId,
    ref: "Time",
  },
  date: {
    type: String,
    required: true,
  },
});

const timeModel = mongoose.model(
  "PreviousAppointment",
  previousAppointmentSchema,
);
module.exports = timeModel;
