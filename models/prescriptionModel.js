const mongoose = require("mongoose");
const { ObjectId } = mongoose.Schema;

const prescriptionSchema=new mongoose.Schema(
    {
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    phone: {type: String, required: true},
    rollNumber: {type: String, required: true},
    problem: {type: String, required:true},
    problem_description: {type:String},
    solution: {type:String,required:true},
    history: {type:String},
    userId: {type:ObjectId,ref:"User"},
    doctorId: {type:ObjectId,ref:"Doctor"},
    timeSlot: {type:ObjectId,ref:"Time"}
    },
    { timestamps: true }
)

const prescriptionModel = mongoose.model("Prescription", prescriptionSchema);
module.exports = prescriptionModel;