const express=require('express');
const router=express.Router();
const bcrypt=require("bcryptjs");
const axios = require("axios")
const jwt = require("jsonwebtoken")
const config = require("config")
const User=require("../models/userModel");
const Doctor=require("../models/doctorModel");
const Prescription=require("../models/prescriptionModel");
const Time=require("../models/timeModel");
const PreviousAppointment = require("../models/previousAppointmentModel");
const authMiddleware=require("../middlewares/authMiddleware");
const multer = require("multer");
const cloudinary = require("cloudinary").v2;
const { CloudinaryStorage } = require("multer-storage-cloudinary");
const ObjectId = require('mongoose').Types.ObjectId;
const { sendEmail } = require('../services/email.service');



  cloudinary.config({
    cloud_name: process.env.CLOUD_NAME,
    api_key: process.env.API_KEY,
    api_secret: process.env.API_SECRET
  });

  const storage = new CloudinaryStorage({
    cloudinary: cloudinary,
    params: {
      folder: "folder",
      format: async () => "png",
      public_id: (req, file) => {
        file.filename
      },
    },
  });
  
const parser = multer({ storage: storage });

router.post("/get-doctor-by-id",authMiddleware,parser.single("image"),async(req,res)=>{
    try {
        const user=await User.findOne({_id:req.userId})
        if(!user)
        {
            return res.status(200).send({message:"user does not exists"})
        }
        else{
            if(user.role!==1)
            {
                return res.status(200).send({message:"doctor does not exist"});
            }
            else
            {
              if(req.body.status==='' || req.file.path==='')
              {
                return res.status(200).send({message:"send all fields"})
              }
              const doct=await Doctor.findOne({userId:user._id});
              if(doct)
                {
                    return res.status(400).send({message:"doctor already exists"})
                }
                dd=await Doctor.create({userId:user._id,details:req.file.path,status:req.body.status})
                const admin=await User.findOne({role:2})
                const unseenNotifications=admin.unseenNotifications
                unseenNotifications.push({
                  type:"new-doctor-request",
                  
                  data:{
                    doctorId:dd._id,
                    name:user.firstName+" "+user.lastName
                  },
                  onClick:"/admin/doctors"
                })
                await User.findByIdAndUpdate(admin._id,{unseenNotifications})
                return res.status(200).send({data:dd,success:true,message:"created doctor"})
            }
       }
    }
    catch(err)
    {
        console.log(err)
        return res.status(500).send({message:"something went wrong",success:false})
    }
})

//route to edit status
router.put("/edit-status-doctor",authMiddleware,async(req,res)=>{
  try{
    const {status}=req.body;
    if(status==='')
    {
      return res.status(200).send({message:"put status",success:false})
    }

    const doct=await Doctor.findOneAndUpdate({userId:req.userId},{$set:{status}},{new:true})
  
      return res.status(200).send({message:"updated successfully",data:doct,success:true})
  }
  catch(err)
  {
    console.log(err)
    return res.status(500).send({message:"something went wrong",success:false})
  }
})

router.post("/get-doctor-info-by-userid",authMiddleware,async(req,res)=>{
  try{
    const doctor=await Doctor.findOne({userId:req.body.userId}).populate("userId","firstName lastName")
    return res.status(200).send({success:true,message:"doctor info fetched successfully",data:doctor})
  }catch(err)
  {
    console.log(err)
    return res.status(500).send({message:"error getting doctor info",success:false})
  }
})

router.post("/edit-doctor-details",authMiddleware,parser.single("image"),async(req,res)=>{
  try{
    const getDc=await Doctor.findOne({userId:req.userId})
    let flag=0;
    if(getDc.details===req.body.image)
    {
      flag=1
    }
    if(req.body.firstName || req.body.lastName)
    {
      getUser=await User.findOneAndUpdate({_id:req.userId},{$set:{
        firstName:req.body.firstName,
        lastName:req.body.lastName
      }},{new:true})
    }
    const getDoct=await Doctor.findOneAndUpdate({userId:req.userId},{$set:{
      status:req.body.status,
      details:flag===1 ? req.body.details : req.file.path 
    }},{new:true})

    const getnewDoct=await Doctor.findOne({userId:req.userId}).populate("userId","firstName lastName")
     return res.status(200).send({message:"updated successfully",data:getnewDoct,success:true})

  }catch(err)
  {
    console.log(err)
    return res.status(500).send({message:"failed to update",success:false})
  }
})

router.post("/create-timings",authMiddleware,async(req,res)=>{
   if(req.body.to && req.body.from)
   {
    const timeg=await Time.find();
    let tg="";
    if(timeg.length>0)
    {
      timeg.map((data)=>{
        if(data.from===req.body.from && data.to===req.body.to)
         {
           tg=data._id;
           return 0;
         }
      })
    }
    if(ObjectId.isValid(tg))
    {
      const newgetDoct=await Doctor.findOneAndUpdate({userId:req.userId},
        {
          $push:{appointment:tg}
        },
        {new:true})
        return res.status(200).send({success:true,message:"value placed",data:newgetDoct})
    }
    else
    {
      const newtg=await Time.create({verified:"true",to:req.body.to,from:req.body.from})
      const newgetDoct=await Doctor.findOneAndUpdate({userId:req.userId},
        {
          $push:{appointment:newtg._id}
        },
        {new:true})
        return res.status(200).send({success:true,message:"time created and value placed",data:newgetDoct})
    }
   }
   else{
    return res.status(500).send({message:"enter to and from for time create",success:false})
   }
})

router.post("/check-present-timings",authMiddleware,async(req,res)=>{
  try{
    const getDoct=await Doctor.findOne({userId:req.userId}).populate("appointment","to from")
    let flag=0;
    if(req.body.to && req.body.from)
    {
      const val=getDoct.appointment
      if(val.length>0)
      {
        val.map((data)=>{
           if(data.from===req.body.from && data.to===req.body.to)
           {
            flag=1;
            return 0;
           }
        })
      }
      if(flag==1)
      {
        return res.status(200).send({message:"already exixts",success:false})
      }
      else{
        return res.status(200).send({message:"not exixts",success:true})
      }
    }
    else{
      return res.status(200).send({message:"enter to and from",success:false})
    }

  }catch(err)
  {
    console.log(err)
    return res.status(500).send({message:"failed to create timings",success:false})
  }
})

//get all timings of doctor
router.post("/get-all-timings",authMiddleware,async(req,res)=>{
  try{
    const getDoct=await Doctor.findOne({userId:req.userId}).populate("appointment","to from");
    return res.status(200).send({message:"data fetched successfully",success:true,data:getDoct})
  }
  catch(err)
  {
    console.log(err)
    return res.status(500).send({message:"something went wrong",success:false})
  }
  


})

//pull timing from doctor
router.post("/delete-time",authMiddleware,async(req,res)=>{
  try{
    console.log(req.body)
   const deldata=await Doctor.findOneAndUpdate({userId:req.userId},{
    $pull:{appointment:req.body.dd}
   },{
    new:true
   })
   return res.status(200).send({message:"deleted successfully",success:true,data:deldata})
  }
  catch(err)
  {
    return res.status(500).send({message:"failed to delete",success:false})
  }
})

router.post("/delete-doctor-appointment", authMiddleware, async (req, res) => {
  try {
      //timeId -> timeId, userId -> respective booked appointment user's id
      const { timeId, userId } = req.body;
      const doctor = await Doctor.findOne({ userId: req.userId }).populate("userId");
      if (!doctor) return res.status(200).send({message: "doctor not found!"});
      const user = await User.findById(userId);

      const doc1 = await Doctor.findByIdAndUpdate(doctor._id, {
          $pull: {
              bookedAppointment: { timeSlot: timeId, user: user._id }
          }
      }, { new: true });
      const doc2 = await User.findByIdAndUpdate(userId, {
          $pull: {
              appointments: {
                  doctor: doctor._id,
                  timeSlot: timeId,
              }
          }
      }, { new: true });

      //remove from previous appointments
      const d = new Date(Date.now());
      const doc3 = await PreviousAppointment.deleteOne({
        doctor: doctor._id,
        user: userId,
        time: timeId,
        date: d.toLocaleDateString(),
      });

      //send emails
      const time = await Time.findById(timeId);
      await sendEmail({to: doctor.userId.email, subject: `Appointment has been invoked`, html: `<b>Slot [${time.to} - ${time.from}] has been removed. Contact Email: ${user.email}</b>`});
      await sendEmail({to: user.email, subject: `Appointment has been invoked by Dr. ${doctor.userId.firstName}`, html: `<b>Appointment with Dr. ${doctor.userId.firstName} ${doctor.userId.lastName}, Slot: [${time.to} - ${time.from}] has been removed by the doctor. Doctor Contact Email: ${doctor.userId.email}</b>`});
      return res.status(200).send({ doc1, doc2, doc3, message: "Appointment Invoked!" });
  } catch (error) {
      console.log(error);
      res.status(500).send({message: "server error"});
  }
});

  router.post("/accept-timing", authMiddleware, async (req, res) => {
    try {
      //userId is the user requested time change, timeId is the prev booked timeId
      const { timeId, doctorId, userId, to, from, type } = req.body;
      if (type !== "accept") return res.status(200).send({ message: 'only for accept timing' });

      const doctor = await Doctor.findById(doctorId).populate("userId");
      const bookingUser = await User.findById(userId);

      //check doctor time is already reserved by someone else 
      const reserved = doctor.bookedAppointment.find((a) => a.timeSlot.to === to && a.timeSlot.from === from);
      if (reserved) return res.status(200).send({ message: 'you already have a active reservation on this time!' });

      const prevTime = await Time.findById(timeId);
      const time = await Time.findOne({ to, from });
      if (!time) return res.status(200).send({ message: 'you do not have a time slot on the provided time!' });

      const update_doctor_pull = await Doctor.findByIdAndUpdate(doctorId, {
        $pull: {
          bookedAppointment: {
            timeSlot: timeId,
            user: userId,
          }
        },
      }, { new: true });

      const update_doctor_push = await Doctor.findByIdAndUpdate(doctorId, {
        $push: {
          bookedAppointment: {
            timeSlot: time._id,
            user: userId,
          }
        }
      }, { new: true });

      const update_user_pull = await User.findByIdAndUpdate(userId, {
        $pull: {
          appointments: {
            doctor: doctorId,
            timeSlot: timeId,
          }
        },
      }, { new: true });

      const update_user_push = await User.findByIdAndUpdate(userId, {
        $push: {
          appointments: {
            doctor: doctorId,
            timeSlot: time._id,
          }
        }
      }, { new: true });

      //Update previous appointment
      const d = new Date(Date.now());
      const update_previous_appointment = await PreviousAppointment.findOneAndUpdate({
        doctor: doctorId,
        user: userId,
        time: timeId,
        date: d.toLocaleDateString(),
      }, {
        $set: {
          time: time._id
        }
      }, { new: true })

      //send emails
      await sendEmail({ to: doctor.userId.email, subject: `Time slot has been updated for Mr/Miss ${bookingUser.firstName}`, html: `<b>Time Slot has been changed from [${prevTime.to} - ${prevTime.from}] to [${time.to} - ${time.from}]</b>` });
      await sendEmail({ to: bookingUser.email, subject: `Time change request has been accepted by Dr. ${doctor.userId.firstName}`, html: `<b>Time Slot has been changed from [${prevTime.to} - ${prevTime.from}] to [${time.to} - ${time.from}]</b>` });

      return res.status(200).send({ update_doctor: {update_doctor_pull, update_doctor_push}, update_user: {update_doctor_pull, update_doctor_push}, update_previous_appointment, message: "success" });
    } catch (error) {
      console.log(error);
      res.status(500).send({message: "server error"});
    }
  });

  router.post("/reject-timing", authMiddleware, async (req, res) => {
    try {
      //userId is the user requested time change, timeId is the prev booked timeId
      const { timeId, doctorId, userId, to, from, type } = req.body;
      if (type !== "reject") return res.status(200).send({ message: 'only for reject timing' });

      const bookingUser = await User.findById(userId);
      const doctor = await Doctor.findById(doctorId).populate("userId");
      const prevTime = await Time.findById(timeId);

      await sendEmail({ to: doctor.userId.email, subject: `Rejected time change request for Mr/Miss ${bookingUser.firstName}`, html: `<b>Time Slot change has been rejected from [${prevTime.to} - ${prevTime.from}] to [${to} - ${from}].<br />Contact Email: ${bookingUser.email}</b>` });
      await sendEmail({ to: bookingUser.email, subject: `Time change request has been rejected by Dr. ${doctor.userId.firstName}`, html: `<b>Time Slot change has been rejected from [${prevTime.to} - ${prevTime.from}] to [${to} - ${from}], you can either request for a different slot or kindly follow the older one.<br />Doctor Contact Email: ${doctor.userId.email}</b>` });

      return res.status(200).send({ message: "success" });
    } catch (error) {
      console.log(error);
      res.status(500).send({message: "server error"});
    }
  })

 //create-prescription
 router.post("/create-prescription",authMiddleware,async(req,res)=>{
  try
  {
    const {firstName,lastName,phone,rollNumber,problem,solution,userId,doctorId,problem_description,history,timeId}=req.body;
    //console.log(req.body)
    if(!firstName || !lastName ||!phone ||!rollNumber ||!problem ||!solution ||!userId ||!doctorId ||!timeId)
    {
      return res.status(200).send({message:"enter all the details",success:false})
    }
    const newpres=await Prescription.create({verified:"true",firstName,lastName,phone,rollNumber,problem,problem_description,solution,userId,doctorId,history,timeSlot:timeId})
    return res.status(200).send({message:"created successfully",success:true,data:newpres})
  }
  catch (error) {
    console.log(error);
    return res.status(500).send({message: "server error",success:false});
  }
})

//get data by timeslot and userid and doctorid
router.post("/get-prescription-by-id",authMiddleware,async(req,res)=>{
 try{
  const {doctId,datas}=req.body
  const alldata=await Prescription.find();
  let c=0;
  let senddata=[]
  datas.map((vals)=>{
    
     if(alldata.length>0)
     {
      alldata.map((vvals)=>{
        if (vvals.userId.toString()=== vals.user._id && vvals.doctorId.toString()===doctId && vvals.timeSlot.toString()===vals.timeSlot._id)
        {
         senddata.push(vvals._id);
         c=1;
        }
      })
      if(c==0)
        {
          senddata.push(0);
        }
        else
        {
          c=0;
        }
     }
     else{
      senddata.push(0);
     }
 })
  // const presdata=await Prescription.findOne({timeSlot:timeId,userId,doctorId:doctId})
  return res.status(200).send({message:"success",success:true,data:senddata})
 }
 catch(err)
 {
  console.log(err)
  return res.status(500).send({message:"unable to find",success:false})
 }

})

router.post("/get-prescription-one",authMiddleware,async(req,res)=>{
  try{
    const data=await Prescription.findById({_id:req.body.id})
    return res.status(200).send({message:"found",success:true,data:data})
  }
  catch(err)
  {
    console.log(err)
    return res.status(500).send({message:"not found",success:false})
  }
})

//edit-prescription
router.post("/edit-prescription",authMiddleware,async(req,res)=>{
  try
  {
    const {firstName,lastName,phone,rollNumber,problem,solution,userId,doctorId,problem_description,history,timeSlot}=req.body;
    const getPrescription=await Prescription.findByIdAndUpdate({_id:req.body.id},{$set:{
      firstName,
      lastName,
      phone,
      rollNumber,
      problem,
      problem_description,
      solution,
      doctorId,
      history,
      userId,
      timeSlot
    }},{new:true})
    return res.status(200).send({message:"updated successfully",success:true,data:getPrescription})
  }
  catch(err)
  {
    console.log(err)
    return res.status(500).send({message:"something went wrong",success:false})
  }
})

//delete-prescription
router.delete("/delete-prescription",authMiddleware,async(req,res)=>{
  try
  {
   const delpres=Prescription.findByIdAndDelete({_id:req.body.id})
   return res.status(200).send({message:"deleted successfully",success:true}) 
  }
  catch(err)
  {
    console.log(err)
    return res.status(500).send({success:false,message:"something went wrong"})
  }
})

router.get("/doctor-previous-appointments", authMiddleware, async (req, res) => {
  try {
    const doctor = await Doctor.findOne({userId: req.userId})
    const appointments = await PreviousAppointment.find({ doctor: doctor._id }).populate({ path: "doctor", populate: { path: "userId", model: "User" }}).populate("user time");;
    return res.status(200).send({appointments, message: "success"});
  } catch (err) {
    console.log(err)
    return res.status(500).send({success:false,message:"server error"})
  }
});
//get for user(all prescription)
router.post("/get-all-prescriptions-user",authMiddleware,async(req,res)=>{
  try{
    if(req.body.doctorId!==undefined)
    {
      const findD=await Doctor.findOne({userId:req.body.doctorId})
      const pudata=await Prescription.find({doctorId:findD._id}).populate("doctorId","userId").populate("timeSlot","to from")
      return res.status(200).send({success:true,message:"fetched successfully",data:pudata})
    }
    const pudata=await Prescription.find({userId:req.userId}).populate("doctorId","userId").populate("timeSlot","to from")
    return res.status(200).send({success:true,message:"fetched successfully",data:pudata})
  }
  catch(err)
  {
    console.log(err)
    return res.status(500).send({success:false,message:"fetched unsuccessfully"})
  }
})

//check for role
router.post("/check-role",authMiddleware,async(req,res)=>{
  try{
    const checkData=await User.findById({_id:req.userId})
    if(checkData && checkData.role===1)
    {
      return res.status(200).send({success:true,data:true})
    }
    else
    {
      return res.status(200).send({success:true,data:false})
    }

  }
  catch(err)
  {
    console.log(err)
    return res.status(500).send({message:"failed to load",success:false})
  }
})

//get for doctor(all prescription)
router.post("/get-all-prescriptions-user",authMiddleware,async(req,res)=>{
  try{
    const checkrole=await User.findById({_id:req.userId})
    if(checkrole && checkrole.role===1)
    {
      const pudata=await Prescription.find({doctorId:req.body.doctorId})
    }
    const pudata=await Prescription.find({doctorId:req.body.doctorId})
    return res.status(200).send({success:true,message:"fetched successfully",data:pudata})
  }
  catch(err)
  {
    console.log(err)
    return res.status(500).send({success:false,message:"fetched unsuccessfully"})
  }
})

module.exports=router;