const express = require("express");
const router = express.Router();
const User = require("../models/userModel");
const Doctor = require("../models/doctorModel");
const PreviousAppointment = require("../models/previousAppointmentModel");
const authMiddleware = require("../middlewares/authMiddleware");

const monthNames = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

router.post("/get-all-doctors", authMiddleware, async (req, res) => {
  try {
    const user = await User.findOne({ _id: req.userId });
    if (user.role === 2) {
      const doctors = await Doctor.find().populate("userId");
      return res.status(200).send({
        success: true,
        message: "doctors fetched successfully",
        sucess: true,
        data: doctors,
      });
    }
    return res
      .status(200)
      .send({ success: false, message: "restricted not admin" });
  } catch (err) {
    console.log(err);
    res.status(500).send({
      message: "error getting all doctors",
      success: false,
    });
  }
});

router.post("/get-all-users", authMiddleware, async (req, res) => {
  try {
    const user = await User.findOne({ _id: req.userId });
    if (user.role === 2) {
      const users = await User.find({ role: 0 });
      return res.status(200).send({
        success: true,
        message: "users fetched successfully",
        sucess: true,
        data: users,
      });
    }
    return res
      .status(200)
      .send({ success: false, message: "restricted not admin" });
  } catch (err) {
    console.log(err);
    res.status(500).send({
      message: "error getting all users",
      success: false,
    });
  }
});

router.post("/clear-day-appointments", authMiddleware, async (req, res) => {
  try {
    const doctor = await Doctor.find();
    const user = await User.find();
    //delete each doctor booked appointments
    doctor.forEach(async (d) => {
      await Doctor.findByIdAndUpdate(
        d._id,
        {
          $set: {
            bookedAppointment: [],
          },
        },
        { new: true },
      );
    });
    //delete each user appointments
    user.forEach(async (u) => {
      await User.findByIdAndUpdate(
        u._id,
        {
          $set: {
            appointments: [],
          },
        },
        { new: true },
      );
    });
    return res.status(200).send({ message: "success" });
  } catch (err) {
    console.log(err);
    res.status(500).send({
      message: "server error",
      success: false,
    });
  }
});

router.get("/previous-appointments", authMiddleware, async (req, res) => {
  try {
    const appointments = await PreviousAppointment.find()
      .populate({ path: "doctor", populate: { path: "userId", model: "User" } })
      .populate("user time");
    return res.status(200).send({ appointments, message: "success" });
  } catch (err) {
    console.log(err);
    res.status(500).send({
      message: "server error",
      success: false,
    });
  }
});

router.get("/analyze-doctor", authMiddleware, async (req, res) => {
  try {
    const { doctorId } = req.query;
    const doctorAppointments = await PreviousAppointment.find({
      doctor: doctorId,
    });
    const doctor = await Doctor.findById(doctorId).populate("userId");
    const monthlyAppointments = {
      January: 0,
      February: 0,
      March: 0,
      April: 0,
      May: 0,
      June: 0,
      July: 0,
      August: 0,
      September: 0,
      October: 0,
      November: 0,
      December: 0,
    };
    doctorAppointments.forEach((appointment) => {
      const month = new Date(appointment.date).getMonth();
      monthlyAppointments[monthNames[month]] += 1;
    });
    console.log(doctorAppointments);
    return res.status(200).send({ monthlyAppointments, doctor, success: true });
  } catch (error) {
    console.log(error);
    res.status(500).send({
      message: "server error",
      success: false,
    });
  }
});

module.exports = router;
