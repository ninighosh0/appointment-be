const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const axios = require("axios");
const jwt = require("jsonwebtoken");
const config = require("config");
const User = require("../models/userModel");
const Doctor = require("../models/doctorModel");
const PreviousAppointment = require("../models/previousAppointmentModel");
const authMiddleware = require("../middlewares/authMiddleware");
const { sendEmail } = require("../services/email.service");
const Time = require("../models/timeModel");

router.post("/register", async (req, res) => {
  try {
    if (req.body.googleAccessToken) {
      const { googleAccessToken } = req.body;
      axios
        .get("https://www.googleapis.com/oauth2/v3/userinfo", {
          headers: {
            Authorization: `Bearer ${googleAccessToken}`,
          },
        })
        .then(async (response) => {
          const firstName = response.data.given_name;
          const lastName = response.data.family_name;
          const email = response.data.email;
          const picture = response.data.picture;
          const existingUser = await User.findOne({ email });
          if (existingUser)
            return res.status(400).json({ message: "User already exist!" });

          const result = await User.create({
            verified: "true",
            email,
            firstName,
            lastName,
            profilePicture: picture,
          });
          const token = jwt.sign(
            {
              email: result.email,
              id: result._id,
            },
            process.env.SECRET,
            { expiresIn: "1h" },
          );
          res.status(200).json({
            result,
            token,
            success: true,
            message: "successfully signed up!",
          });
        })
        .catch((err) => {
          res.status(400).json({ message: "Invalid access token!" });
        });
    } else {
      const { email, password, firstname, lastname } = req.body;

      try {
        if (
          email === "" ||
          password === "" ||
          firstname === "" ||
          (lastname === "" && password.length <= 4)
        )
          return res.status(400).json({ message: "Invalid field!" });

        const existingUser = await User.findOne({ email });

        if (existingUser)
          return res.status(400).json({ message: "User already exist!" });

        const hashedPassword = await bcrypt.hash(password, 12);

        const result = await User.create({
          email,
          password: hashedPassword,
          firstName: firstname,
          lastName: lastname,
        });

        const token = jwt.sign(
          {
            email: result.email,
            id: result._id,
          },
          process.env.SECRET,
          { expiresIn: "1h" },
        );

        res.status(200).json({
          result,
          token,
          success: true,
          message: "successfully signed up!",
        });
      } catch (err) {
        res
          .status(500)
          .json({ message: "Something went wrong!", success: false });
      }
    }
  } catch (err) {
    return res
      .status(500)
      .send({ message: "error creating user", success: false });
  }
});

router.post("/login", async (req, res) => {
  try {
    if (req.body.googleAccessToken) {
      // gogole-auth
      const { googleAccessToken } = req.body;

      axios
        .get("https://www.googleapis.com/oauth2/v3/userinfo", {
          headers: {
            Authorization: `Bearer ${googleAccessToken}`,
          },
        })
        .then(async (response) => {
          const email = response.data.email;
          const existingUser = await User.findOne({ email });

          if (!existingUser)
            return res
              .status(404)
              .json({ message: "User don't exist!", success: false });

          const token = jwt.sign(
            {
              email: existingUser.email,
              id: existingUser._id,
            },
            process.env.SECRET,
            { expiresIn: "1h" },
          );

          res.status(200).json({
            result: existingUser,
            token,
            success: true,
            message: "successfully signed in!",
          });
        })
        .catch((err) => {
          res.status(400).json({ message: "Invalid access token!" });
        });
    } else {
      // normal-auth
      const { email, password } = req.body;
      if (email === "" || password === "")
        return res.status(400).json({ message: "Invalid field!" });
      try {
        const existingUser = await User.findOne({ email });

        if (!existingUser)
          return res.status(404).json({ message: "User don't exist!" });

        const isPasswordOk = await bcrypt.compare(
          password,
          existingUser.password,
        );

        if (!isPasswordOk)
          return res.status(400).json({ message: "Invalid credintials!" });

        const token = jwt.sign(
          {
            email: existingUser.email,
            id: existingUser._id,
          },
          process.env.SECRET,
          { expiresIn: "1h" },
        );

        res.status(200).json({
          result: existingUser,
          token,
          success: true,
          message: "successfully signed in!",
        });
      } catch (err) {
        res
          .status(500)
          .json({ message: "Something went wrong!", success: false });
      }
    }
  } catch (err) {
    return res
      .status(500)
      .send({ message: "error creating user", success: false });
  }
});

router.post("/get-user-by-id", authMiddleware, async (req, res) => {
  try {
    const user = await User.findOne({ _id: req.userId })
      .populate({
        path: "appointments",
        populate: {
          path: "doctor",
          model: "Doctor",
          populate: [
            { path: "userId", model: "User" },
            { path: "appointment", model: "Time" },
          ],
        },
      })
      .populate({
        path: "appointments",
        populate: { path: "timeSlot", model: "Time" },
      });
    if (!user) {
      return res.status(200).send({ message: "user does not exists" });
    } else {
      return res.status(200).send({ success: true, data: user });
    }
  } catch (err) {
    return res
      .status(500)
      .send({ message: "something went wrong", success: false });
  }
});

//route to mark notification as seen
router.post(
  "/mark-all-notifications-as-seen",
  authMiddleware,
  async (req, res) => {
    try {
      const user = await User.findOne({ _id: req.body.userId });
      console.log(req.body.userId);
      const unseenNotifications = user.unseenNotifications;
      user.seenNotifications = unseenNotifications;
      user.unseenNotifications = [];
      const updateUser = await User.findByIdAndUpdate(user._id, user);
      updateUser.password = undefined;
      res.status(200).send({
        success: true,
        message: "all notifications cleared",
        data: updateUser,
      });
    } catch (err) {
      console.log(err);
      res.status(500).send({ message: "error in clearing notifications" });
    }
  },
);

router.post("/delete-all-notifications", authMiddleware, async (req, res) => {
  try {
    const user = await User.findOne({ _id: req.body.userId });
    user.seenNotifications = [];
    user.unseenNotifications = [];
    const updateUser = await User.findByIdAndUpdate(user._id, user);
    updateUser.password = undefined;
    res.status(200).send({
      success: true,
      message: "all notifications cleared",
      data: updateUser,
    });
  } catch (err) {
    console.log(err);
    res.status(500).send({ message: "error in clearing notifications" });
  }
});

//Create appointment
router.post("/create-appointment", authMiddleware, async (req, res) => {
  try {
    const { doctorId, timeId, userId } = req.body;
    const user = await User.findOne({ _id: userId });
    const allUserAppointments = user.appointments;
    //Check for existing appoinments
    const existingAppointment = allUserAppointments.find(
      (a) =>
        a.doctor.toString() === doctorId && a.timeSlot.toString() === timeId,
    );
    if (existingAppointment)
      return res.status(200).send({ message: "appointment already exists!" });
    const doctor = await Doctor.findOne({ userId: doctorId }).populate(
      "userId",
    );
    if (!doctor) return res.status(200).json({ message: "doctor not found!" });

    if (doctor.userId === user._id)
      return res.status(200).json({ message: "cannot reserve yourself!" });

    if (!doctor) return res.status(200).json({ message: "doctor not found!" });
    //Check for slot availability
    const slot = doctor.appointment.find((a) => a.toString() === timeId);
    if (!slot)
      return res
        .status(200)
        .json({ message: "selected slot is not available!" });

    //Check for already booked slot
    const isBooked = doctor.bookedAppointment.find(
      (a) => a.toString() === timeId,
    );
    if (isBooked)
      return res
        .status(200)
        .json({ message: "selected slot is already booked!" });

    // Create new apointment
    const newAppointment = { doctor: doctor._id, timeSlot: timeId };
    const doc1 = await User.findByIdAndUpdate(
      user._id,
      {
        $push: {
          appointments: newAppointment,
        },
      },
      { new: true },
    );
    //Reserve doctor
    const doc2 = await Doctor.findByIdAndUpdate(
      doctor._id,
      {
        $push: {
          bookedAppointment: {
            timeSlot: timeId,
            user: userId,
          },
        },
      },
      { new: true },
    );
    //Add to previous appointment for history
    const d = new Date(Date.now());
    const doc3 = await PreviousAppointment.create({
      user: req.userId,
      doctor: doctor._id,
      time: timeId,
      date: d.toLocaleDateString(),
    });

    //Send emails
    //doctor
    const time = await Time.findOne({ _id: timeId });
    await sendEmail({
      to: doctor.userId.email,
      subject: `Appointment booked by ${user.firstName}`,
      html: `<b>Slot [${time.to} - ${time.from}] has been booked by Mr/Miss ${user.firstName} ${user.lastName}. Contact Email: ${user.email}</b>`,
    });
    //user
    await sendEmail({
      to: user.email,
      subject: `Appointment Created`,
      html: `<b>Appointment with Dr. ${doctor.userId.firstName} ${doctor.userId.lastName} has been created. Doctor Contact Email: ${doctor.userId.email}</b>`,
    });
    return res
      .status(200)
      .send({ doc1, doc2, doc3, message: "Appointment created successfully!" });
  } catch (error) {
    console.log(error);
    res.status(500).send({ message: "server error" });
  }
});

router.post("/delete-appointment", authMiddleware, async (req, res) => {
  try {
    //doctorId -> respective appointment Doctor's userId, userId -> logged in userId
    const { doctorId, timeId } = req.body;
    const doc1 = await Doctor.findByIdAndUpdate(
      doctorId,
      {
        $pull: {
          bookedAppointment: { timeSlot: timeId, user: req.userId },
        },
      },
      { new: true },
    );
    const doc2 = await User.findByIdAndUpdate(
      req.userId,
      {
        $pull: {
          appointments: {
            doctor: doctorId,
            timeSlot: timeId,
          },
        },
      },
      { new: true },
    );

    //remove from previous appointments
    const d = new Date(Date.now());
    const doc3 = await PreviousAppointment.deleteOne({
      doctor: doctorId,
      user: req.userId,
      time: timeId,
      date: d.toLocaleDateString(),
    });

    //send emails
    const doctor = await Doctor.findById(doctorId).populate("userId");
    const user = await User.findById(req.userId);
    const time = await Time.findById(timeId);

    await sendEmail({
      to: doctor.userId.email,
      subject: `Appointment has been invoked by Mr/Miss ${user.firstName}`,
      html: `<b>Slot [${time.to} - ${time.from}] has been removed by Mr/Miss ${user.firstName} ${user.lastName}. Contact Email: ${user.email}</b>`,
    });
    await sendEmail({
      to: user.email,
      subject: `Appointment has been invoked`,
      html: `<b>Appointment with Dr. ${doctor.userId.firstName} ${doctor.userId.lastName} has been removed. Doctor Contact Email: ${doctor.userId.email}</b>`,
    });
    return res
      .status(200)
      .send({ doc1, doc2, doc3, message: "Appointment Invoked!" });
  } catch (error) {
    console.log(error);
    res.status(500).send({ message: "server error" });
  }
});

//Get all appointments
router.get("/get-all-appointments", authMiddleware, async (req, res) => {
  try {
    const doctors = await Doctor.find()
      .populate("appointment userId")
      .lean()
      .select("-__v -status -details -createdAt -updatedAt");
    return res
      .status(200)
      .send({ data: doctors, message: "success", success: true });
  } catch (error) {
    console.log(error);
    res.status(500).send({ message: "server error" });
  }
});

//Change appointment timings request
router.post("/request-time", authMiddleware, async (req, res) => {
  try {
    const { to, from, doctorId, timeId } = req.body;
    console.log(to, from, doctorId, timeId);
    if (to && from) {
      const doctor = await Doctor.findById(doctorId).populate("userId");
      const user = await User.findById(req.userId);
      const time = await Time.findById(timeId);
      if (!doctor || !user)
        return res.status(500).send({ message: "doctor or user not found!" });
      const acceptUrl = `http://localhost:3000/doctor/change-timings?to=${to}&from=${from}&prevTimeId=${timeId}&doctorId=${doctorId}&userId=${user._id}&type=accept`;
      const rejectUrl = `http://localhost:3000/doctor/change-timings?to=${to}&from=${from}&prevTimeId=${timeId}&doctorId=${doctorId}&userId=${user._id}&type=reject`;
      await sendEmail({
        to: doctor.userId.email,
        subject: "Request for change of timings",
        html: `<b>Dear Dr. ${doctor.userId.firstName},<br/> Mr/Miss ${user.firstName} has requested the existing slot [${time.to} - ${time.from}] to be changed to [${to} - ${from}]<br /><a href="${acceptUrl}">Accept</a> <a href="${rejectUrl}">Reject</a></b>`,
      });
      return res.status(200).send({ message: "Request email sent to doctor" });
    } else {
      return res.status(500).send({ message: "provide to and from" });
    }
  } catch (error) {
    console.log(error);
    return res.status(500).send({ message: "server error" });
  }
});
//get appointments booked of doctor
router.post(
  "/get-doctor-bookedappointments",
  authMiddleware,
  async (req, res) => {
    try {
      const doctor = await Doctor.findOne({ userId: req.userId }).populate({
        path: "bookedAppointment",
        populate: [
          { path: "user", model: "User" },
          { path: "timeSlot", model: "Time" },
        ],
      });

      return res
        .status(200)
        .send({ data: doctor, message: "success", success: true });
    } catch (err) {
      console.log(err);
      return res.status(500).send({ success: false, message: "server error" });
    }
  },
);

router.get("/user-previous-appointments", authMiddleware, async (req, res) => {
  try {
    const appointments = await PreviousAppointment.find({ user: req.userId })
      .populate({ path: "doctor", populate: { path: "userId", model: "User" } })
      .populate("user time");
    return res.status(200).send({ appointments, message: "success" });
  } catch (err) {
    console.log(err);
    return res.status(500).send({ success: false, message: "server error" });
  }
});

module.exports = router;
