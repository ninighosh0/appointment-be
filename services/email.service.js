const nodemailer = require("nodemailer");

const sendEmail = async ({
  to = "",
  from = "doctorappointment@xyz.in",
  subject = "Hello ✔",
  text = "Hello world?",
  html = "<b>Hello world?</b>",
}) => {
  let transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 587,
    secure: false,
    auth: {
      user: process.env.GMAIL_USER,
      pass: process.env.GMAIL_APP_PASS,
    },
  });

  let info = await transporter.sendMail({
    from,
    to, // list of receivers
    subject,
    text,
    html,
  });

  console.log("Message sent: %s", info.messageId);
  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
};

module.exports = { sendEmail };
